from datetime import datetime
import os
import re
import socket
import socketserver
import sys

from pymongo import MongoClient

DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S,%f'
FILENAME_FORMAT = '%Y_%m_%d'
DEBUG_ENV = 'UDP_SERVER_DEBUG'

class RollingFileHandler(socketserver.BaseRequestHandler):
    def handle(self):
        data = self.request[0].strip().decode()
        socket = self.request[1]
        
        if os.environ.get(DEBUG_ENV, 0):
            print(data)
            return 

        log_event = LogEvent(data)
        log_event.save()

class LogEvent:
    def __init__(self, data):
        directory, raw_string = data.split(' ', 1)
        date, level, context, message = raw_string.split(' ', 3)

        self.directory = directory
        self.raw_string = raw_string
        self.date = datetime.strptime(date, DATETIME_FORMAT)
        self.level = level
        self.context = self.parse_context_into_dict(context)
        self.message = message

    def parse_context_into_dict(self, context):
        values = context.split(',')
        return {
            'msisdn': values[0],
            'request_id': values[1]
        }

    def save(self):
        self._write_to_database()
        self._write_to_filesystem()

    def _write_to_database(self):
        client = MongoClient('mongodb://127.0.0.1/zapzap')
        db = client.zapzap
        db.log_event.insert_one({
            'directory': self.directory,
            'date': self.date,
            'level': self.level,
            'context': self.context,
            'message': self.message
        })

    def _write_to_filesystem(self):
        file_name = datetime.now().strftime(FILENAME_FORMAT)
        file_uri = 'logs/{}{}.log'.format(self.directory, file_name)

        with open(file_uri, mode='a') as log_file:
            log_file.write(self.raw_string)
            log_file.write('\n')


class UnixDomainSocketServer(socketserver.UnixDatagramServer):
    def server_bind(self):
        LISTEN_FDS = int(os.environ.get('LISTEN_FDS', 0))
        LISTEN_PID = os.environ.get('LISTEN_PID', None) or os.getpid()
        SD_LISTEN_FDS_START = 3

        if LISTEN_FDS == 0:
            super().server_bind()
        else:
            print('creating socket from file descriptor (fd)')
            self.socket = socket.fromfd(
                SD_LISTEN_FDS_START, 
                self.address_family, 
                self.socket_type
            )


if __name__ == '__main__':
    UNIX_DOMAIN_SOCKET_FILE_PATH = sys.argv[1]
    server = UnixDomainSocketServer(UNIX_DOMAIN_SOCKET_FILE_PATH, RollingFileHandler)
    
    try:
        print('listening for datagrams on unix domain socket {}...'.format(UNIX_DOMAIN_SOCKET_FILE_PATH))
        server.serve_forever()
    except KeyboardInterrupt:
        print('shutting down server...')
        server.shutdown()
